package com.gitee.kamismile.stoneComEx.common.component.support.annotation;

import org.springframework.web.bind.annotation.Mapping;

import java.lang.annotation.*;

@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Mapping
@Documented
public @interface SecurityParameter {


    boolean inDecode() default false;


    boolean outEncode() default false;
}
