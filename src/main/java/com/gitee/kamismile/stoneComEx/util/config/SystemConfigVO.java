package com.gitee.kamismile.stoneComEx.util.config;

import java.io.Serializable;

public class SystemConfigVO implements Serializable {
	private static final long serialVersionUID = -5454356037840325865L;
	private String type;
	private int step;
	private int layer;
	public int getLayer() {
		return layer;
	}
	public void setLayer(int layer) {
		this.layer = layer;
	}
	public int getStep() {
		return step;
	}
	public void setStep(int step) {
		this.step = step;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

}
