package com.gitee.kamismile.stoneComEx.util.view;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by lidong on 15-7-24.
 */
public class ModelBean {
    private String viewCtrl;   //展示类型

    private List<String[][]> muchColmons=new ArrayList<String[][]>();   //   {"sWnadId","广告编号" }  ,
    private List<List<Map<String,Object>>> maps=new ArrayList<List<Map<String,Object>>>();
    public List<String[][]> getMuchColmons() {
        return muchColmons;
    }

    public void setColmons(String[][] colmons) {
        muchColmons.add(colmons);
    }

    public List<List<Map<String,Object>>> getMaps() {
        return maps;
    }

    public void setMaps(List<Map<String, Object>> maps) {
        this.maps.add(maps);
    }

    public String getViewCtrl() {
        if(viewCtrl==null)
            return "generalExcel";
        return viewCtrl;
    }

    public void setViewCtrl(String viewCtrl) {
        this.viewCtrl = viewCtrl;
    }


}
