package com.gitee.kamismile.stoneComEx.util.webDriver;

import com.gitee.kamismile.stoneComEx.common.Constant;
import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.FileUrlResource;
import org.springframework.core.io.Resource;

import javax.imageio.ImageIO;
import java.io.File;
import java.util.concurrent.TimeUnit;

public class WebDriverJSClientPoolFactory extends BasePooledObjectFactory<RemoteWebDriver> {
    protected final Logger log =LoggerFactory.getLogger(getClass());


    @Value("${spring.remoteweb.path:-1}")
    Resource path;

    @Value("${spring.remoteweb.minIdle:3}")
    private int minIdle=3;
    @Value("${spring.remoteweb.maxIdle:5}")
    private int maxIdle=5;


    public Resource getPath() {
        return path;
    }

    public void setPath(Resource path) {
        this.path = path;
    }

    @Override
    public RemoteWebDriver create() throws Exception {
        DesiredCapabilities caps = new DesiredCapabilities();
//        caps.setJavascriptEnabled(true); // enabled by default
        caps.setBrowserName(Constant.GOOGLE);
        if(!"-1".equals(path.getURL().getPath())){
            caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,path.getURL().getFile());
        }
        caps.setCapability("phantomjs.page.settings.userAgent", Constant.GOOGLE);
        caps.setCapability("page.settings.userAgent", Constant.GOOGLE);

        PhantomJSDriver webDriver = new PhantomJSDriver(caps);
        webDriver.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);
        webDriver.manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);
        webDriver.manage().timeouts().pageLoadTimeout(8, TimeUnit.SECONDS);
        return webDriver;
    }



    @Override
    public PooledObject<RemoteWebDriver> wrap(RemoteWebDriver value) {
        return new DefaultPooledObject<>(value);
    }

//    @Bean(name="remoteWebPool",autowire= Autowire.BY_NAME)
    @Bean(name="remoteWebPool")
    protected GenericObjectPool<RemoteWebDriver> getPhantomJSPool(){
        WebDriverJSClientPoolFactory phantomJSClientPoolFactory=new WebDriverJSClientPoolFactory();
        phantomJSClientPoolFactory.setPath(path);

        GenericObjectPoolConfig gopc=new GenericObjectPoolConfig();
        gopc.setJmxEnabled(false);
        GenericObjectPool<RemoteWebDriver> phantomJSPool = new GenericObjectPool<RemoteWebDriver>
                (phantomJSClientPoolFactory,gopc);
        phantomJSPool.setMinIdle(this.minIdle);//3个对象
        phantomJSPool.setMaxIdle(this.maxIdle);
        return phantomJSPool;
    }


    public static void main(String[] args) throws Exception {
        WebDriverJSClientPoolFactory bb = new WebDriverJSClientPoolFactory();
        bb.setPath(new FileUrlResource("J:/qqDownload/phantomjs.exe"));
        RemoteWebDriver webDriver=bb.getPhantomJSPool().borrowObject();
        webDriver.manage().window().setSize(new Dimension(1024,768));
        webDriver.get("file:///J://qqDownload/hetong.html");
        File file = webDriver.getScreenshotAs(OutputType.FILE);


//                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ImageIO.write(        ImageIO.read(file), "png", new File("d:/a.png"));
    }
}
