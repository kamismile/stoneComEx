package com.gitee.kamismile.stoneComEx.common.filter.server;

import org.springframework.context.i18n.LocaleContext;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilterChain;
import org.springframework.web.server.i18n.LocaleContextResolver;
import reactor.core.publisher.Mono;


public class LocaleChangeReactiveFilter implements ReactiveFilter {
    private LocaleContextResolver localeContextResolver;

    public LocaleChangeReactiveFilter(LocaleContextResolver localeContextResolver) {
        this.localeContextResolver = localeContextResolver;
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
        LocaleContext localeContext = localeContextResolver.resolveLocaleContext(exchange);
        localeContextResolver.setLocaleContext(exchange,localeContext);

        exchange.getResponse().beforeCommit(() -> {
            LocaleContextHolder.resetLocaleContext();
            return Mono.empty();
        });
        return chain.filter(exchange);
    }
}
