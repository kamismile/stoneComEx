package com.gitee.kamismile.stoneComEx.common;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Import(App.class)
public @interface EnableApp {
}
