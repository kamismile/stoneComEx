package com.gitee.kamismile.stoneComEx.common.persist.exception;

/**
 * 对数据库进行insert,update,delete后，没有数据被影响，则可以抛出此异常。
 * 调用者可以根据需要捕获或放弃此异常。
 *
 *
 */
public class NoRecordAffectedException extends RuntimeException {

	private static final long serialVersionUID = -2784886993466826976L;

	public NoRecordAffectedException(String message) {
		super(message);
	}

	public NoRecordAffectedException(String message, Throwable cause) {
		super(message, cause);
	}

}
