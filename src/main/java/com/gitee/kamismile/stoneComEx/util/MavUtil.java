package com.gitee.kamismile.stoneComEx.util;

import com.gitee.kamismile.stone.commmon.base.ResultVO;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import java.util.HashMap;
import java.util.Map;

public class MavUtil {
	private static String viewName = "error";

	public static ModelAndView errMav(int code,String message){
		ResultVO rest = new ResultVO(String.valueOf(code), message);
		ModelAndView mav = new ModelAndView(MavUtil.viewName,"error",rest);
		return mav;
	}

	public static ModelAndView errMav(int code){
		String message = In18Util.getError(code);
		ResultVO rest = new ResultVO(String.valueOf(code), message);
		ModelAndView mav = new ModelAndView(MavUtil.viewName,"error",rest);
		return mav;
	}

	public static ModelAndView jsonOut(int code,String message,Object object) throws Exception{
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("code", code);
		map.put("message", message);
		map.put("success", true);
		if(code==-1)
			map.put("success", false);

		if(object != null){
			map.put("extend", object);
		}

		MappingJackson2JsonView view = new MappingJackson2JsonView();
		view.setContentType("text/html");
		view.setAttributesMap(map);

		return new ModelAndView(view);
	}

	public static ModelAndView jsonOut(int code,String message) throws Exception{
		return jsonOut(code,message,null);
	}

	public static ModelAndView viewOut(String key,Object obj){
		ModelAndView mav = new ModelAndView();
		mav.addObject(key, obj);
		return mav;
	}
}
