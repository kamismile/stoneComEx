/**
 * LY.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package com.gitee.kamismile.stoneComEx.common.component.servlet;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.servlet.LocaleResolver;

import java.util.Locale;

/**
 * @author dong.li
 * @version $Id: ServletLocaleResolver, v 0.1 2019/11/7 18:13 dong.li Exp $
 */
public class ServletLocaleResolver implements LocaleResolver {

    @Value("${spring.language:zh_CN}")
    private String lang;

    @Override
    public Locale resolveLocale(HttpServletRequest httpServletRequest) {
        String language = httpServletRequest.getHeader("custom-Language");
        Locale targetLocale =  Locale.forLanguageTag(lang);
        if (!StringUtils.isEmpty(language)) {
            targetLocale = Locale.forLanguageTag(language);
        }
        return targetLocale;
    }

    @Override
    public void setLocale(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Locale locale) {
//        throw new UnsupportedOperationException("Not Supported");
        LocaleContextHolder.setLocale(locale);
    }
}
