package com.gitee.kamismile.stoneComEx.util.view.excelView;

import org.springframework.web.servlet.view.AbstractView;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: lidong
 * Date: 12-3-10
 * Time: 上午11:24
 * 返回excel视图
 */
public class ExcelViewUtil {


    /**
     * @param templateName 模版名称
     * @return  excel视图
     */

    public static AbstractView   getExcelView(String  templateName){
        Map map=new HashMap();
        map.put("GeneralExcelView",new GeneralExcelView());
        map.put("DeatilReportExcelView",new DeatilReportExcelView());

        Object obj = map.get(templateName);
        if(obj==null){
            return null;
        }
        return (AbstractView) obj;
    }

}
