package com.gitee.kamismile.stoneComEx.util;

/**
 * Created by wangjie3 on 2015/12/30.
 */
public class ValidateUtil {



    public static boolean validateExPass(String data){
        return data.matches("(?=.*\\d)(?=.*[A-z])^[0-9A-z]{8,}$");

    }


    public static boolean isMobileNO(String data){
    	boolean isMobileNO = false;
    	if (StringUtil.isNotEmpty(data)) {
    		isMobileNO = data.matches("^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$");
		}

        return isMobileNO;
    }

}
