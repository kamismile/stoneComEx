package com.gitee.kamismile.stoneComEx.common.exception.servlet;

import com.gitee.kamismile.stone.commmon.base.ResultVO;
import com.gitee.kamismile.stone.commmon.util.ValueUtils;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

//import org.springframework.boot.autoconfigure.web.ErrorController;

/**
 * Created by lidong on 15-11-10.
 */
@Controller
//public class OtherErrorController extends BasicErrorController {
public class OtherErrorController implements ErrorController {

    private static final String ERROR_PATH = "/error";
//
//    public OtherErrorController(ErrorAttributes errorAttributes, ServerProperties serverProperties) {
//        super(errorAttributes, serverProperties.getError());
//    }

//    @RequestMapping(value = ERROR_PATH)
//    @ResponseBody
//    public ResponseEntity<Map<String, Object>> error(HttpServletRequest req) {
//        Map<String, Object> body = getErrorAttributes(req,
//                isIncludeStackTrace(req, MediaType.ALL));
//
//        Map<String, Object> result=new HashMap<>();
//        result.put("code",ValueUtils.isStringNull(req.getAttribute(ServletErrorEnum.CODE.getTypeName()),"-2"));
//        result.put("message","");
////        ResultVO errorMsg = new ResultVO();
////        errorMsg.setCode(ValueUtils.isStringNull(req.getAttribute(ServletErrorEnum.CODE.getTypeName()),"-2"));
////        errorMsg.setMessage("page not found");
//
//        return new ResponseEntity<Map<String, Object>>(result, HttpStatus.resolve(ValueUtils.
//                isIntegerNull(req.getAttribute(ServletErrorEnum.CODE.getTypeName()),
//                        HttpStatus.OK.value()))) ;
//    }

    @RequestMapping(value=ERROR_PATH)
    public ResponseEntity<Map> handleError(HttpServletRequest req){
        ResultVO errorMsg = new ResultVO();
        errorMsg.setCode(ValueUtils.isStringNull(req.getAttribute(ServletErrorEnum.CODE.getTypeName()),"-1"));
        errorMsg.setMessage("");

        Map responseMap = new HashMap();
        responseMap.put("code", errorMsg.getCode());
        responseMap.put("data", errorMsg.getData());
        responseMap.put("message", errorMsg.getMessage());
        responseMap.put("errors", Arrays.asList(errorMsg));

        return new ResponseEntity<Map>(responseMap, HttpStatus.resolve(ValueUtils.
                isIntegerNull(req.getAttribute(ServletErrorEnum.CODE.getTypeName()),
                        HttpStatus.OK.value()))) ;
    }

    public String getErrorPath() {
        // TODO Auto-generated method stub
        return ERROR_PATH;
    }
}
