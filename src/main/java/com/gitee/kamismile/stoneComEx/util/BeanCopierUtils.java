/**
 * LY.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package com.gitee.kamismile.stoneComEx.util;

import com.gitee.kamismile.stoneComEx.util.converter.beancopier.DateConverterBeanCopier;
import com.gitee.kamismile.stone.commmon.util.ValueUtils;
import org.springframework.cglib.beans.BeanCopier;

/**
 * @author dong.li
 * @version $Id: BeanCopierUtils, v 0.1 2019/3/22 16:05 dong.li Exp $
 */
public class BeanCopierUtils {

    public static void copyProperties(Object source, Object target, String... timeStamp) {
        BeanCopier copier = BeanCopier.create(source.getClass(), target.getClass(), true);
        DateConverterBeanCopier dtConverter = new DateConverterBeanCopier();
        if (ValueUtils.isNotNull(timeStamp)) {
            dtConverter.setPatterns(timeStamp);
        }
        copier.copy(source, target, dtConverter);
    }


}
