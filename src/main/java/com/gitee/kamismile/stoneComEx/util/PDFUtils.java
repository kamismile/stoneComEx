package com.gitee.kamismile.stoneComEx.util;

import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.html2pdf.resolver.font.DefaultFontProvider;
import com.itextpdf.io.font.FontProgram;
import com.itextpdf.io.font.FontProgramFactory;
import com.itextpdf.layout.font.FontProvider;

import java.io.File;
import java.io.IOException;

public class PDFUtils {

    public static void writePdf(String html, String pdf) throws IOException {
        ConverterProperties properties = new ConverterProperties();
        properties.setCharset("UTF-8");
        FontProvider fontProvider = new DefaultFontProvider();
        FontProgram fontProgram = FontProgramFactory.createFont("fonts/simsun.ttf");
        fontProvider.addFont(fontProgram);
        properties.setFontProvider(fontProvider);
        HtmlConverter.convertToPdf(new File(html), new File(pdf), properties);
    }


    public static void main(String[] args) throws IOException {


        writePdf("F:\\qqDownload\\hetong2.html", "F:\\qqDownload\\a.pdf");

    }


}
