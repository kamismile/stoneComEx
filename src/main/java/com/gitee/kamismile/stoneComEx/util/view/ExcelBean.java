package com.gitee.kamismile.stoneComEx.util.view;

import java.net.URI;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by IntelliJ IDEA.
 * User: lidong
 * Date: 12-3-10
 * Time: 下午2:48
 * To change this template use File | Settings | File Templates.
 */
public class ExcelBean extends ModelBean {

    private String sheetName;
    private String fileName;   //下载的文件名
    private boolean waterMark = false; //是否需要打印水印
    private String  waterMarkMsg; //水印需要打印的内容
    private URI waterMarkMsgURI;//水印图片在excel中的路径

    public ExcelBean(){

    }

    public ExcelBean(String waterMarkMsg){
    	this.waterMarkMsg = waterMarkMsg;
    	this.waterMark = true;
    }

    public URI getWaterMarkMsgURI() {
		return waterMarkMsgURI;
	}

	public void setWaterMarkMsgURI(URI waterMarkMsgURI) {
		this.waterMarkMsgURI = waterMarkMsgURI;
	}

	public boolean isWaterMark() {
		return waterMark;
	}

	public void setWaterMark(boolean waterMark) {
		this.waterMark = waterMark;
	}

	public String getWaterMarkMsg() {
		return waterMarkMsg;
	}

	public void setWaterMarkMsg(String waterMarkMsg) {
		this.waterMarkMsg = waterMarkMsg;
	}

	public String getSheetName() {
    	//清除掉所有特殊字符
    	String regex = "[`~!@#$%^&*()/+,-.:;<=>?{}\\[\\]]";
	    Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(sheetName);
        return m.replaceAll("_");
    }

    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }


}
