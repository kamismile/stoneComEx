package com.gitee.kamismile.stoneComEx.util.view.excelView;

import com.gitee.kamismile.stoneComEx.util.view.ExcelBean;
import com.gitee.kamismile.stoneComEx.common.component.base.AbstractXSSFExcelView;
import com.gitee.kamismile.stoneComEx.util.ExcelUtil;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: lidong
 * Date: 12-3-10
 * Time: 上午11:32
 * 一行一列
 */
public class GeneralExcelView extends AbstractXSSFExcelView {


    @Override
    protected void buildExcelDocument(
            Map<String, Object> model,
            Workbook workbook,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        List<ExcelBean> excelBeans = new ArrayList<ExcelBean>();


        if(model.get("excelBean")!=null){
            excelBeans.add((ExcelBean) model.get("excelBean"));
        }else{
            excelBeans.addAll((List<ExcelBean>) model.get("excelBeans"));
        }

        new ExcelUtil().expDatas((SXSSFWorkbook) workbook, excelBeans);

        String fileName=excelBeans.get(0).getFileName();
        if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {
            fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
        }
        else{
            fileName = URLEncoder.encode(fileName, "UTF-8");
        }

        response.setHeader("Pragma","No-cache");
        response.setHeader("Cache-Control","no-cache");
        response.setDateHeader("Expires", 0);
        response.setHeader("Content-Disposition", MessageFormat.format("attachment; filename={0}.xlsx",
                fileName ));
    }
}
