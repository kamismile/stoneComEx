package com.gitee.kamismile.stoneComEx.util;


import jakarta.servlet.http.HttpServletResponse;

public class ThreadLocalResponseUtil {
	private static ThreadLocal<HttpServletResponse> responses = new ThreadLocal<HttpServletResponse>();

	public  static void setResponse(HttpServletResponse response){
		if(response!=null){
			responses.set(response);
		}
	}

	public  static HttpServletResponse getResponse(){
		HttpServletResponse response = responses.get();
		return response;
	}

	public  static void removeResponse(){
		responses.remove();
	}
}
