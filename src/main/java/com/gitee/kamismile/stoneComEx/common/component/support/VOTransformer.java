package com.gitee.kamismile.stoneComEx.common.component.support;
import com.gitee.kamismile.stoneComEx.util.BeanUtils;
import com.gitee.kamismile.stone.commmon.util.ValueUtils;
import org.apache.commons.collections.Transformer;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.io.Serializable;
/**
 * Created by lidong on 15-11-10.
 */
public class VOTransformer implements Transformer, Serializable {
    protected final Logger logger =LoggerFactory.getLogger(getClass());
    private Class classVO;
    private String timeStampVO;

    public static Transformer getInstance(Class aClass) throws Exception {
        return new VOTransformer(aClass);
    }

    public static Transformer getInstance(Class aClass,String timeStamp) throws Exception {
        return new VOTransformer(aClass,timeStamp);
    }



    private VOTransformer(Class aClass) throws Exception {
        classVO=aClass;
    }
    private VOTransformer(Class aClass,String timeStamp) throws Exception {
        timeStampVO=timeStamp;
        classVO=aClass;
    }

    @Override
    public Object transform(Object input) {
        Object objVO =null;

        try {
            objVO = classVO.getDeclaredConstructor().newInstance();

            if(ValueUtils.isNotNull(timeStampVO)){
                BeanUtils.copyPropert(objVO, input,timeStampVO);
            } else {
                BeanUtils.copyPropert(objVO, input);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return objVO;  //To change body of implemented methods use File | Settings | File Templates.

    }
}
