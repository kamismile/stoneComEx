package com.gitee.kamismile.stoneComEx.common.component.support.annotation;

import org.springframework.web.bind.annotation.Mapping;

import java.lang.annotation.*;

@Target({ElementType.FIELD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Mapping
@Documented
public @interface XssParameter {


    boolean xss() default true;
}
