package com.gitee.kamismile.stoneComEx.util;
import jakarta.servlet.http.HttpServletRequest;

import java.io.*;
import java.util.HashMap;
//import com.khan.net.*;


/*
  version 1.0
  修正了传递二进制文件不正常的问题, 因为我将流转成了字符串的方式解析, 导致二进制文件的不可见字符转码失败
  解决, 全部采用byte解析
  暂时还是只能上传单个文件
  支持传递文件的同时, 参数url参数
*/
public class UploadFileTest  {
    public static final int MAX_SIZE = 1024 * 1024*100;
    public static final String FILE_DIR = "/opt/resource/uploadFiles";

    private static  int file_Size=0;
    private static String file_Path = "";
    private static HashMap hm = new HashMap();

    public static String upLoad(HttpServletRequest req, InputStream in) {
        String tmpString ="";
        String result = "";
        DataInputStream dis = null;

        try {
            dis = new DataInputStream(in);
            String content = req.getContentType();
            if (content != null && content.indexOf("multipart/form-data") != -1) {

                int reqSize = req.getContentLength();
                byte[] data = new byte[reqSize];

                int bytesRead = 0;
                int totalBytesRead = 0;
                int sizeCheck = 0;
                while (totalBytesRead < reqSize) {
                    // check for maximum file size violation
                    sizeCheck = totalBytesRead + dis.available();
                    if (sizeCheck > MAX_SIZE)
                        result = "文件太大不能上传...";

                    bytesRead = dis.read(data, totalBytesRead, reqSize);
                    totalBytesRead += bytesRead;
                }

                tmpString = new String(data);
                hm = parseAnotherParam(tmpString);
//System.out.println("src datatmp |"+new String(data)+"|");
                int postion = arrayIndexOf(data, "\r\n".getBytes());
                byte[] split_arr = new byte[postion];
                System.arraycopy(data, 0, split_arr, 0, postion);
//System.out.println("split |"+new String(split_arr)+"|");

                postion = arrayIndexOf(data, "filename=\"".getBytes());
                byte[] dataTmp = new byte[data.length - postion];
                System.arraycopy(data, postion, dataTmp, 0, dataTmp.length);
                data = null;
                data = dataTmp.clone();


                String filePath =null;
                postion = arrayIndexOf(data, "Content-Type:".getBytes())-2;
                dataTmp = null;
                dataTmp = new byte[postion];
                System.arraycopy(data, 0, dataTmp, 0, dataTmp.length);
                filePath = new String(dataTmp);
                if (filePath==null && filePath.equals("")) return "";
//System.out.println("filename |"+filePath+"|");
               // 分离contentType 并赋值
                postion = arrayIndexOf(data, "Content-Type:".getBytes());
                dataTmp = null;
                dataTmp = new byte[data.length - postion];
                System.arraycopy(data, postion, dataTmp, 0, dataTmp.length);
                data = null;
                data = dataTmp.clone();
//System.out.println("src adatatmp |"+new String(data)+"|");

                postion = arrayIndexOf(data, "\n".getBytes()) + 1;
                dataTmp = null;
                dataTmp = new byte[data.length - postion];
                System.arraycopy(data, postion, dataTmp, 0, dataTmp.length);
                data = null;
                data = dataTmp.clone();
//System.out.println("datatmp |"+new String(data)+"|");

                // 分离文件信息 获得最终想要的字节
                postion = arrayIndexOf(data, split_arr);
                split_arr = null;
                dataTmp = null;
                dataTmp = new byte[postion - 2];
                System.arraycopy(data, 2, dataTmp, 0, dataTmp.length);
                data = null;
                data = dataTmp.clone();
//System.out.println("datatmp |"+new String(data)+"|");

                postion = arrayLastIndexOf(data, "\n".getBytes())-1;
                dataTmp = null;
                dataTmp = new byte[postion];
//System.out.println("postion:"+postion + " datalength:"+ data.length +" tmplength:" + dataTmp.length);
                System.arraycopy(data, 0, dataTmp, 0, dataTmp.length);

                data = null;
//System.out.println("data |"+new String(dataTmp)+"|");
                String file_path = getFileName(filePath);
//System.out.println("file_path:"+file_path);
                if(null != file_path) {
                  if (writeFile(dataTmp, FILE_DIR + file_path)) {
                    file_Size = dataTmp.length;
                    file_Path = FILE_DIR + file_path;
                    result = "文件上传完毕";
                  } else {
                    result = "文件上传失败";
                  }
                }else{
                    result = "文件名为空";
                }
                dataTmp = null;
            } else {
                result = "content 必须为 multipart/form-data";
            }
        } catch (UnsupportedEncodingException ex4) {
            result = "UnsupportedEncodingException错误";
        } catch (NullPointerException e) {
            result = "NullPointerException错误";
        } catch (IOException ex1) {
            result = "IOException 错误 ";
        }catch (Exception ex1) {
            result = "Exception 错误 ";
        }

        return result;
    }

    public String getFilePath(){
        return this.file_Path;
    }

    public int getFileSize(){
        return this.file_Size;
    }

    public static boolean writeFile(byte[] data, String path) {
        File f = null;
        FileOutputStream fos = null;
        try {
            f = new File(path);
            f.createNewFile();
            fos = new FileOutputStream(f);
            fos.write(data, 0, data.length);
        } catch (FileNotFoundException e) {
            return false;
        } catch (IOException e) {
            return false;
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                return false;
            }
        }
        return true;
    }

    public static String getFileName(String arg) {
        String path = "";
        if(arg.equals("\"\"")) {
            return null;
        }

        if (arg.indexOf("\"") > -1)
            path = arg.substring(arg.indexOf("\"") + 1, arg.lastIndexOf("\""));
        else
            path = arg;
    //System.out.println("file_path:"+arg);
        path = path.substring(path.lastIndexOf("\\") + 1);
        return path;
    }


    //判断两个byte数组的值是否相等
    private static boolean arrayEquals(byte[] src, byte[] value){
        if(src == null || value == null)
            return false;
        if(src.length != value.length)
            return false;

        for(int i=0; i<src.length; i++) {
            if(src[i] != value[i])
                return false;
        }
        return true;
    }


    //找出value数组在src中的位置, 从前往后
    private static int arrayIndexOf(byte[] src, byte[] value){
        if(src == null || value == null)
            return -1;
        if(src.length < value.length)
            return -1;

        int postion = -1;

        for(int i=0; i<src.length - value.length; i++) {
            postion = i;
            byte[] tmp = new byte[value.length];
            System.arraycopy(src, i, tmp, 0, tmp.length);
            if(arrayEquals(tmp, value)) {
                tmp = null;
                return postion;
            }else{
                postion = -1;
                tmp = null;
            }
        }

        return postion;
    }


    //找出value数组在src中的位置
    private static int arrayLastIndexOf(byte[] src, byte[] value){
        if(src == null || value == null)
            return -1;
        if(src.length < value.length)
            return -1;

        int postion = -1;

        for(int i=src.length - value.length ; i >-1; i--) {
            postion = i;

            byte[] tmp = new byte[value.length];
            System.arraycopy(src, i, tmp, 0, tmp.length);
//System.out.println(i);
//Common.PrintDataHex(tmp, " ");
//Common.PrintDataHex(value, " ");

            if(arrayEquals(tmp, value)) {
                tmp = null;
                return postion;
            }else{
                postion = -1;
                tmp = null;
            }
        }
        //System.out.println("debug");
        return postion;
    }


    public static HashMap parseAnotherParam(String str){
      HashMap<String, String> hm= new HashMap<String, String>();
      String key="";
      String value="";
      int startindex = 0;
      int endindex = 0;

      startindex = str.indexOf("Content-Disposition: form-data; name=\"")
                 + "Content-Disposition: form-data; name=\"".length();
      endindex = str.indexOf("\"\r\n\r\n");

      while ( startindex >-1 && endindex > -1 ){
        key = str.substring(startindex, endindex);

        if(!str.substring(endindex , endindex + 5).equals("\"\r\n\r\n")  ){//去掉没有value的元素
            str = str.substring(endindex);
            startindex = str.indexOf("Content-Disposition: form-data; name=\"")
                       + "Content-Disposition: form-data; name=\"".length();
            endindex = str.indexOf("\"\r\n\r\n");
            continue;
        }
        if( key.indexOf("\";") > -1){//去掉上传文件的参数以及编码
           str = str.substring(str.indexOf("\";") + 2);
           startindex = str.indexOf("Content-Disposition: form-data; name=\"")
                      + "Content-Disposition: form-data; name=\"".length();
           endindex = str.indexOf("\"\r\n\r\n");

           continue;
        } else
            str = str.substring(endindex + 5);

        value = str.substring(0, str.indexOf("\r\n"));
        str = str.substring(str.indexOf("\r\n") + 2);
        //System.out.println("key:"+key+" value:"+value);
        hm.put(key,value);

        startindex = str.indexOf("Content-Disposition: form-data; name=\"")
                   + "Content-Disposition: form-data; name=\"".length();
        endindex = str.indexOf("\"\r\n\r\n");

      }
      return hm;
    }

    public static String getParameter(String param){
        //System.out.println(hm.toString());
      return (String)hm.get(param);
    }




}
