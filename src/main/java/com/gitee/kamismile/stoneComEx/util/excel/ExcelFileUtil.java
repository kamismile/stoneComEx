package com.gitee.kamismile.stoneComEx.util.excel;

import java.io.File;
import java.io.FileOutputStream;
import java.util.*;

import com.gitee.kamismile.stone.commmon.util.ValueUtils;
import org.apache.commons.io.IOUtils;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import com.gitee.kamismile.stoneComEx.util.ExcelUtil;
import com.gitee.kamismile.stoneComEx.util.view.ExcelBean;

public class ExcelFileUtil {

    /**
     * 私有化构造函数
     */
    private ExcelFileUtil() {
    }


    public static void exportData(ExcelBean excelBean, String filePath, int excleNum) {
        // 生成Excel文件
        SXSSFWorkbook workbook = new SXSSFWorkbook(10000);
        workbook.setCompressTempFiles(true);
        new ExcelUtil()
                .setExcleNum(excleNum)
                .expData(workbook, excelBean);
        FileOutputStream fout = null;
        // 保存Excel文件到服务器上
        try {
            // 判断目录是否存在，不存在则创建目录
            File file = new File(filePath).getParentFile();
            if (!file.exists()) {
                file.mkdirs();
            }
            fout = new FileOutputStream(filePath);
            workbook.write(fout);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            workbook.dispose();
            if (ValueUtils.isNotNull(fout)) {
                IOUtils.closeQuietly(fout);
            }
        }
    }


    public static void exportDatas(List<ExcelBean> excelBeans, String filePath) {
        // 生成Excel文件
        SXSSFWorkbook workbook = new SXSSFWorkbook(100);
        workbook.setCompressTempFiles(true);
        new ExcelUtil().expDatas(workbook, excelBeans);
        // 保存Excel文件到服务器上
        FileOutputStream fout = null;
        try {
            // 判断目录是否存在，不存在则创建目录
            File file = new File(filePath).getParentFile();
            if (!file.exists()) {
                file.mkdirs();
            }
            fout = new FileOutputStream(filePath);
            workbook.write(fout);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            workbook.dispose();
            if (ValueUtils.isNotNull(fout)) {
                IOUtils.closeQuietly(fout);
            }
        }
    }

}
