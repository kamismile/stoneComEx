package com.gitee.kamismile.stoneComEx.common.component.server;

import com.gitee.kamismile.stoneComEx.common.component.support.BaseController;
import com.gitee.kamismile.stone.commmon.base.ResultVO;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.util.MultiValueMap;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class SimpleFluxController extends BaseController {
    protected final Logger logger =LoggerFactory.getLogger(this.getClass());

    protected ResultVO getResultVO() {
        return getResultVO("0");
    }

    protected ResultVO getResultVO(String code) {
        return getResultVO(code, "");
    }

    protected ResultVO getResultVO(String code, String msg) {
        ResultVO vo = null;
        try {
            vo = super.getResultVO(code, msg, null);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return vo;
    }

    protected <T> ResultVO<T> successfulResultVO(T result) {
        ResultVO vo = null;
        try {
            vo = super.getResultVO("0", "操作成功", result);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return vo;
    }

    protected ResultVO failureResultVO(String message) {
        ResultVO vo = null;
        try {
            vo = super.getResultVO("-1", message, null);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return vo;
    }

    public Map<String, String> getRequestMap(ServerHttpRequest serverHttpRequest) {
        MultiValueMap<String, String> parameterMap = serverHttpRequest.getQueryParams();
        Iterator<String> it = parameterMap.keySet().iterator();
        Map<String, String> map = new HashMap<String, String>();
        while (it.hasNext()) {
            String objs = (String) it.next();
            List<String> obj = parameterMap.get(objs);
            map.put(objs, StringUtils.join(obj, ","));
        }

        return map;
    }
}
