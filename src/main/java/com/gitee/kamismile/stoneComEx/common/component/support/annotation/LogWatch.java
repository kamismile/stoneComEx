package com.gitee.kamismile.stoneComEx.common.component.support.annotation;

import java.lang.annotation.*;

/**
 * Created by lidong on 2017/2/20.
 */
@Inherited
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LogWatch {
    String value() default "";

    //1打印结果
    int result() default 1;
}
