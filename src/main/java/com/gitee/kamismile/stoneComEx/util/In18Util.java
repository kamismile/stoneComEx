package com.gitee.kamismile.stoneComEx.util;

import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.web.util.WebUtils;

import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: lidong
 * Date: 12-11-2
 * Time: 下午5:03
 * To change this template use File | Settings | File Templates.
 */
public class In18Util {

    public void setMessageSource(ReloadableResourceBundleMessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public static ReloadableResourceBundleMessageSource messageSource;

    public static String getError(int code){
        Locale locale = (Locale) WebUtils.getSessionAttribute((((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest()),
                SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME);
        return messageSource.getMessage("id_" + code, null,"", locale);
    }

}
