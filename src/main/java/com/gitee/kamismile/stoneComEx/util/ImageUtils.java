package com.gitee.kamismile.stoneComEx.util;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 图片操作工具类
 *
 * @author wangjie3
 * @since 2016-1-15
 */

public class ImageUtils {

    //图片压缩
    private File file = null; // 文件对象
    private String inputDir; // 输入图路径
    private String outputDir; // 输出图路径
    private String inputFileName; // 输入图文件名
    private String outputFileName; // 输出图文件名
    private int outputWidth = 110; // 默认输出图片宽
    private int outputHeight = 110; // 默认输出图片高
    private boolean proportion = true; // 是否等比缩放标记(默认为等比缩放)

    public ImageUtils() { // 初始化变量
        inputDir = "";
        outputDir = "";
        inputFileName = "";
        outputFileName = "";
        outputWidth = 110;
        outputHeight = 110;
    }

    public void setInputDir(String inputDir) {
        this.inputDir = inputDir;
    }

    public void setOutputDir(String outputDir) {
        this.outputDir = outputDir;
    }

    public void setInputFileName(String inputFileName) {
        this.inputFileName = inputFileName;
    }

    public void setOutputFileName(String outputFileName) {
        this.outputFileName = outputFileName;
    }

    public void setOutputWidth(int outputWidth) {
        this.outputWidth = outputWidth;
    }

    public void setOutputHeight(int outputHeight) {
        this.outputHeight = outputHeight;
    }

    public void setWidthAndHeight(int width, int height) {
        this.outputWidth = width;
        this.outputHeight = height;
    }

    /*
    * 获得图片大小
    * 传入参数 String path ：图片路径
    */
    public long getPicSize(String path) {
        file = new File(path);
        return file.length();
    }

           // 图片处理
    public String compressPic() {
        try {
           //获得源文件
            file = new File(inputDir + inputFileName);
            if (!file.exists()) {
                return "";
            }
            Image img = ImageIO.read(file);
            //当图片长和宽都大于默认时不压缩
            if(img.getHeight(null)<=outputHeight||img.getWidth(null)<=outputWidth){
                return "";
            }
            System.out.println(img.getWidth(null)+"||"+img.getHeight(null));
           // 判断图片格式是否正确
            if (img.getWidth(null) == -1) {
                System.out.println(" can't read,retry!" + "<BR>");
                return "no";
            } else {
                int newWidth;
                int newHeight;
           // 判断是否是等比缩放
                if (this.proportion == true) {
           // 为等比缩放计算输出的图片宽度及高度
                    double rate1 = ((double) img.getWidth(null)) / (double) outputWidth ;
                    double rate2 = ((double) img.getHeight(null)) / (double) outputHeight ;
            // 根据缩放比率大的进行缩放控制
                    double rate = rate1 < rate2 ? rate1 : rate2;
                    newWidth = (int) (((double) img.getWidth(null)) / rate);
                    newHeight = (int) (((double) img.getHeight(null)) / rate);
                } else {
                    newWidth = img.getWidth(null); // 输出的图片宽度
                    newHeight = img.getHeight(null); // 输出的图片高度
                }
                BufferedImage tag = new BufferedImage((int) newWidth, (int) newHeight, BufferedImage.TYPE_INT_RGB);
            /*
            * Image.SCALE_SMOOTH 的缩略算法 生成缩略图片的平滑度的
            * 优先级比速度高 生成的图片质量比较好 但速度慢
            */
                tag.getGraphics().drawImage(img.getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH), 0, 0, null);
                FileOutputStream out = new FileOutputStream(outputDir + outputFileName);
                String formatName = outputFileName.substring(outputFileName.lastIndexOf(".") + 1);
                ImageIO.write(tag, formatName,out);
                out.close();

            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return "ok";
    }

    public String compressPic(String inputDir, String outputDir, String inputFileName, String outputFileName) {
   // 输入图路径
        this.inputDir = inputDir;
   // 输出图路径
        this.outputDir = outputDir;
   // 输入图文件名
        this.inputFileName = inputFileName;
   // 输出图文件名
        this.outputFileName = outputFileName;
        return compressPic();
    }

    public static String compressPic(String inputDir, String outputDir, String inputFileName, String outputFileName, int width, int height, boolean gp) {
        ImageUtils imageUtils=new ImageUtils();
        // 输入图路径
        imageUtils.inputDir = inputDir;
    // 输出图路径
        imageUtils.outputDir = outputDir;
    // 输入图文件名
        imageUtils.inputFileName = inputFileName;
    // 输出图文件名
        imageUtils.outputFileName = outputFileName;
    // 设置图片长宽
        imageUtils.setWidthAndHeight(width, height);
    // 是否是等比缩放 标记
        imageUtils.proportion = gp;
        return imageUtils.compressPic();
    }
    // main测试
// compressPic(大图片路径,生成小图片路径,大图片文件名,生成小图片文名,生成小图片宽度,生成小图片高度,是否等比缩放(默认为true))



	public static void main(String[] arg) {
        ImageUtils mypic = new ImageUtils();
		System.out.println("输入的图片大小：" + mypic.getPicSize("D:\\test\\gold\\Chrysanthemum.jpg")/1024 + "KB");
		mypic.compressPic("D:\\test\\gold\\", "D:\\test\\gold\\", "Chrysanthemum.jpg", "0000.png", 110, 110, true);
	}


}
