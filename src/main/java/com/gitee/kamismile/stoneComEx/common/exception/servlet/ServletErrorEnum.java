package com.gitee.kamismile.stoneComEx.common.exception.servlet;

public enum ServletErrorEnum {
    CODE("jakarta.servlet.error.status_code"),
    URL("jakarta.servlet.error.request_uri");


    private String typeName;

    ServletErrorEnum(String typeName) {
        this.typeName = typeName;
    }

    public String getTypeName() {
        return typeName;
    }
}
