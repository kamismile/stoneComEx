package com.gitee.kamismile.stoneComEx.common.filter.servlet;


import com.gitee.kamismile.stoneComEx.util.ThreadLocalRequestUtil;
import jakarta.servlet.ServletRequestEvent;
import jakarta.servlet.ServletRequestListener;

public class AppRequestListener implements ServletRequestListener {
    @Override
    public void requestDestroyed(ServletRequestEvent sre) {
        ThreadLocalRequestUtil.removeRequest();
    }
}
