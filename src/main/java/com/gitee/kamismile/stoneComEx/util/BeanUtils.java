package com.gitee.kamismile.stoneComEx.util;

import com.gitee.kamismile.stone.commmon.util.ValueUtils;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.ConvertUtilsBean;
import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.commons.beanutils.converters.*;
import org.apache.commons.beanutils.locale.converters.DateLocaleConverter;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.Map;

/**
 * Created by lidong on 15-11-10.
 */
public class BeanUtils extends org.apache.commons.beanutils.BeanUtilsBean2 {
    /**
     * @param dest 目标对象
     * @param orig 源数据对象
     * @throws Exception 异常
     */
    public static void copyPropert(Object dest, Object orig) throws Exception {
        copyPropert(dest, orig, null);
    }

    /**
     * @param dest      目标对象       将sql.DATE换成unil.Date
     * @param orig      源数据对象
     * @param timeStamp 要变成的日期格式
     * @throws Exception 异常
     */
    public static void copyPropert(Object dest, Object orig, String... timeStamp) throws Exception {
        DateTimeConverter dtConverter = new DateConverter(null);

        if (ValueUtils.isNotNull(timeStamp)) {
            dtConverter.setPatterns(timeStamp);
//            dtConverter.setUseLocaleFormat(true);
        }

        ConvertUtilsBean convertUtilsBean = new ConvertUtilsBean();
        convertUtilsBean.deregister(Date.class);
        convertUtilsBean.register(dtConverter, java.util.Date.class);//字符串转DATE
//        convertUtilsBean.register(new DateLocaleConverter(), java.util.Date.class);
//        convertUtilsBean.register(dtConverter, String.class);//字符串按照日期格式化
//        BigDecimalConverter bd = new BigDecimalConverter(BigDecimal.ZERO);
//        convertUtilsBean.register(bd, java.math.BigDecimal.class);

        IntegerConverter integerConverter = new IntegerConverter(null);
        convertUtilsBean.register(integerConverter, Integer.class);
        LongConverter longConverter = new LongConverter(null);
        convertUtilsBean.register(longConverter, Long.class);
        BeanUtilsBean beanUtilsBean = new BeanUtilsBean(convertUtilsBean, new PropertyUtilsBean());
        beanUtilsBean.copyProperties(dest, orig);
    }


    public static void populate(Object dest, final Map<String, ? extends Object> properties, String... timeStamp) throws Exception {
        DateTimeConverter dtConverter = new DateConverter(null);

        if (ValueUtils.isNotNull(timeStamp)) {
            dtConverter.setPatterns(timeStamp);
        }

        ConvertUtilsBean convertUtilsBean = new ConvertUtilsBean();
        convertUtilsBean.deregister(Date.class);
        convertUtilsBean.register(dtConverter, java.util.Date.class);//字符串转DATE
//        BigDecimalConverter bd = new BigDecimalConverter(BigDecimal.ZERO);
//        convertUtilsBean.register(bd, java.math.BigDecimal.class);
        IntegerConverter integerConverter = new IntegerConverter(null);
        convertUtilsBean.register(integerConverter, Integer.class);
        LongConverter longConverter = new LongConverter(null);
        convertUtilsBean.register(longConverter, Long.class);


        BeanUtilsBean beanUtilsBean = new BeanUtilsBean(convertUtilsBean, new PropertyUtilsBean());
        beanUtilsBean.populate(dest, properties);
    }

    public static Map<String, String> describe(Object dest, String... timeStamp) throws Exception {
        DateTimeConverter dtConverter = new DateConverter(null);

        if (ValueUtils.isNotNull(timeStamp)) {
            dtConverter.setPatterns(timeStamp);
        }

        ConvertUtilsBean convertUtilsBean = new ConvertUtilsBean();
        convertUtilsBean.deregister(Date.class);
        convertUtilsBean.register(dtConverter, java.util.Date.class);//字符串转DATE
//        BigDecimalConverter bd = new BigDecimalConverter(BigDecimal.ZERO);
//        convertUtilsBean.register(bd, java.math.BigDecimal.class);

        IntegerConverter integerConverter = new IntegerConverter(null);
        convertUtilsBean.register(integerConverter, Integer.class);
        LongConverter longConverter = new LongConverter(null);
        convertUtilsBean.register(longConverter, Long.class);
        BeanUtilsBean beanUtilsBean = new BeanUtilsBean(convertUtilsBean, new PropertyUtilsBean());
        Map<String, String> map = beanUtilsBean.describe(dest);
        map.remove("class");
        return map;
    }
}
