package com.gitee.kamismile.stoneComEx.common.persist.base;


import com.gitee.kamismile.stone.commmon.base.pagination.PaginationData;

import java.util.List;
import java.util.Map;

/**
 *
 * 基本的DAO接口
 * @author hefei
 *
 * @param <T>实体类型
 * @param <K>实体的主键类型
 */
public interface IBaseDAO<T,K> {


	int countByCriteria(IModelExample example);


	int add(T record);


	int addSelective(T record);


	List<T> getListByCriteria(IModelExample example);


	List<Map<String,Object>> getListByMap(String statement, Object map);


	T getByPrimaryKey(K id);


	int editByCriteriaSelective(T record,
								IModelExample example);


	int editByCriteria(T record,
					   IModelExample example);


	int editByPrimaryKeySelective(T record);


	int editByPrimaryKey(T record);


	PaginationData<T> getByCriteria(IModelExample example, int pageId, int pageSize);


	PaginationData<T> getByCriteria(IModelExample example, int pageId);

	int deleteByPrimaryKey(K id);


	int deleteByExample(IModelExample example);


	int updateOrAdd(T record, K id);


	int checkWithAdd(T record, K id);


	T getModelByCriteria(IModelExample example);

	List<T> queryListByMap(String statement, Object map);

	public  PaginationData<T> getByMapForInner(final String sel, final Map map, final int pageId,
											   final int pageSize);

	public int deleteBySql(String statement, Map map);

	public int deleteBySql(String statement);

	public int updateBySql(String statement, Map map);
}
