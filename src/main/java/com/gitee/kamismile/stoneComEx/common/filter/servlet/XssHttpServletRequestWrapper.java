package com.gitee.kamismile.stoneComEx.common.filter.servlet;

import com.gitee.kamismile.stone.commmon.util.ValueUtils;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.HtmlUtils;


public class XssHttpServletRequestWrapper extends ContentCachingRequestWrapper {


    public XssHttpServletRequestWrapper(HttpServletRequest request) {
        super(request);
    }

    @Override
    public String getHeader(String name) {
        String value = super.getHeader(name);
        if(ValueUtils.isNotNull(value)) {
            return HtmlUtils.htmlEscape(value);
        }else {
            return value;
        }
    }

    @Override
    public String getParameter(String name) {
        String value = super.getParameter(name);
        if(ValueUtils.isNotNull(value)) {
            return HtmlUtils.htmlEscape(value);
        }else {
            return value;
        }
    }

    @Override
    public String[] getParameterValues(String name) {
        String[] values = super.getParameterValues(name);
        if (values != null) {
            int length = values.length;
            String[] escapseValues = new String[length];
            for (int i = 0; i < length; i++) {
                if(ValueUtils.isNotNull(values[i])) {
                    escapseValues[i] = HtmlUtils.htmlEscape(values[i]);
                }else {
                    escapseValues[i] = values[i];
                }
            }
            return escapseValues;
        }
        return super.getParameterValues(name);
    }

}
