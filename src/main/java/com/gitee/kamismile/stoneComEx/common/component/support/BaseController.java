package com.gitee.kamismile.stoneComEx.common.component.support;

import com.gitee.kamismile.stoneComEx.common.component.support.annotation.Remark;
import com.gitee.kamismile.stoneComEx.common.component.support.annotation.Transient;
import com.gitee.kamismile.stoneComEx.common.persist.base.IHeadModel;
import com.gitee.kamismile.stone.commmon.base.ResultVO;
import com.gitee.kamismile.stone.commmon.base.pagination.PaginationData;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.util.ReflectionUtils;

import java.util.*;

public class BaseController {

    protected <T> ResultVO getResultVO(String code, String msg, T obj) throws IllegalAccessException {
        final ResultVO<T> transfer = new ResultVO<T>(code, msg, obj);
        if (obj instanceof PaginationData) {
            PaginationData paginationData = (PaginationData) obj;
            transfer.setPage(paginationData.getPageInfo());
            if (!paginationData.isShowHead()) {
                transfer.setData((T) paginationData.getData());
            } else {
                Collection data = paginationData.getData();
                showHeadList(transfer, data);
            }
        } else if (obj instanceof Collection) {
            showHeadList(transfer, (Collection) obj);
        } else if (obj instanceof IHeadModel) {
            showHead(transfer, obj, new TranVo() {
                @Override
                public void doWithVO(List list, List headList, List remarkList) throws IllegalAccessException {
                    List fileList = new ArrayList<>();
                    getModelHead(headList, remarkList, obj, fileList, 0);
                    list.add(fileList.toArray());
                }
            });
        }
        return transfer;
    }

    private void showHeadList(ResultVO transfer, Collection data) throws IllegalAccessException {
        showHead(transfer, data, new TranVo() {
            @Override
            public void doWithVO(List list, List headList, List remarkList) throws IllegalAccessException {
                Iterator it = data.iterator();
                int i = 0;
                while (it.hasNext()) {
                    List fileList = new ArrayList<>();
                    Object o = it.next();
                    i = getModelHead(headList, remarkList, o, fileList, i);
                    if (CollectionUtils.isNotEmpty(fileList)) {
                        list.add(fileList.toArray());
                    }
                }
            }
        });
    }

    private void showHead(ResultVO transfer, Object data, TranVo tranVo) throws IllegalAccessException {
        Map<String, Object> stringObjectMap = new HashMap<String, Object>();
        List list = new ArrayList<>();
        List headList = new ArrayList<>();
        List remarkList = new ArrayList<>();
        tranVo.doWithVO(list, headList, remarkList);
        stringObjectMap.put("list", list);
        stringObjectMap.put("head", headList);
        stringObjectMap.put("remark", remarkList);
        if (CollectionUtils.isEmpty(list)) {
            transfer.setData(data);
        } else {
            transfer.setData(stringObjectMap);
        }
    }

    private int getModelHead(List headList, List remarkList, Object o, List fileList, int i) throws IllegalAccessException {
        if (!(o instanceof IHeadModel)) {
            return ++i;
        }

        int finalI = i;
        ReflectionUtils.doWithFields(o.getClass(), (field) -> {
            field.setAccessible(true);
            Remark remark = field.getAnnotation(Remark.class);
            Transient aTransient = field.getAnnotation(Transient.class);
            if (null != aTransient) {
                return;
            }
            if (finalI == 0) {
                headList.add(field.getName());
                String valueRemark = (null != remark) ? remark.value() : "";
                remarkList.add(valueRemark);
            }
            fileList.add(field.get(o));
        });

//            Field[] fields = o.getClass().getDeclaredFields();
//            for(Field field:fields){
//                field.setAccessible(true);
//                Remark remark=field.getAnnotation(Remark.class);
//                Transient aTransient=field.getAnnotation(Transient.class);
//                if(null!=aTransient){
//                    continue;
//                }
//                if (i == 0) {
//                    headList.add(field.getName());
//                    String valueRemark = (null != remark) ? remark.value() : "";
//                    remarkList.add(valueRemark);
//                }
//                fileList.add(field.get(o));
//            }
        return ++i;
    }

    interface TranVo {
        void doWithVO(List list, List headList, List remarkList) throws IllegalAccessException;
    }
}
