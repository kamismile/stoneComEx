package com.gitee.kamismile.stoneComEx.common.persist.base;

import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Interface ISqlMapper only identifies the semantics of being MyBatis's Mappers.
 * @author hefei
 *
 */
public interface ISqlMapper<T,K> {

	int countByExample(IModelExample example);

	int insert(T record);

	int insertSelective(T record);

	List<T> selectByExample(IModelExample example);

	T selectByPrimaryKey(K id);

	int updateByExampleSelective(@Param("record") T record,
								 @Param("example") IModelExample example);

	int updateByExample(@Param("record") T record,
						@Param("example") IModelExample example);

	int updateByPrimaryKeySelective(T record);

	int updateByPrimaryKey(T record);

}
