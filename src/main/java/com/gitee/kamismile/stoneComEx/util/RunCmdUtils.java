package com.gitee.kamismile.stoneComEx.util;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.io.OutputStream;

public class RunCmdUtils {

    private static final Logger logger =LoggerFactory.getLogger(RunCmdUtils.class);

    private RunCmdUtils() {
    }

    public static void run(CommandLine commandLine, final OutputStream out, final OutputStream err) {
        DefaultExecutor executor = new DefaultExecutor();
        PumpStreamHandler streamHandler = new PumpStreamHandler(out,err);
        executor.setStreamHandler(streamHandler);
        try {
            executor.execute(commandLine);
        } catch(Exception e) {
            logger.error(ExceptionUtils.getStackTrace(e));
        }
    }
}
