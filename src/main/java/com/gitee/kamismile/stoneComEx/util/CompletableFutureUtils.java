/**
 * LY.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package com.gitee.kamismile.stoneComEx.util;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

/**
 * @author dong.li
 * @version $Id: CompletableFutureUtils, v 0.1 2018/9/14 10:09 dong.li Exp $
 */
public class CompletableFutureUtils {


    public static <T> CompletableFuture<List<T>> allOf(List<CompletableFuture<T>> futuresList) {
        CompletableFuture<Void> allFuturesResult =
                CompletableFuture.allOf(futuresList.toArray(new CompletableFuture[futuresList.size()]));
        return allFuturesResult.thenApply(v ->
                futuresList.stream().
                        map(future -> future.join()).
                        collect(Collectors.<T>toList())
        );
    }

//    CompletableFuture<List<CarPriceResponseVO>> future = CompletableFuture.supplyAsync(() -> {
//        return didiPrice(carPriceRequestVO, vo.getPassnegerId());
//    }, CarThreadPool.getExecutor());
//            futureList.add(future);
}
