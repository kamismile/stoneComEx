package com.gitee.kamismile.stoneComEx.common;

import com.gitee.kamismile.stoneComEx.common.component.support.server.SimpleFluxWebConfig;
import com.gitee.kamismile.stoneComEx.common.filter.server.LoggingReactiveFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

//@EnableWebFlux
@Import(SimpleFluxWebConfig.class)
@ComponentScan(basePackages = "com.gitee.kamismile.stoneComEx.common.*.server")
public class AppFlux {

    @Bean
    public LoggingReactiveFilter loggingReactiveFilter() {
        return new LoggingReactiveFilter();
    }


}
