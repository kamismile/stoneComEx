package com.gitee.kamismile.stoneComEx.util;

import jakarta.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.support.ServerRequestWrapper;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class IpUtil {
	public static int str2Ip(String ip) throws UnknownHostException {
		InetAddress address = InetAddress.getByName(ip);
		byte[] bytes = address.getAddress();
		int a, b, c, d;
		a = byte2int(bytes[0]);
		b = byte2int(bytes[1]);
		c = byte2int(bytes[2]);
		d = byte2int(bytes[3]);
		int result = (a << 24) | (b << 16) | (c << 8) | d;
		return result;
	}

	public static int byte2int(byte b) {
		int l = b & 0x07f;
		if (b < 0) {
			l |= 0x80;
		}
		return l;
	}

	public static long ip2long(String ip) throws UnknownHostException {
		int ipNum = str2Ip(ip);
		return int2long(ipNum);
	}

	public static long int2long(int i) {
		long l = i & 0x7fffffffL;
		if (i < 0) {
			l |= 0x080000000L;
		}
		return l;
	}

	public static String long2ip(long ip) {
		int[] b = new int[4];
		b[0] = (int) ((ip >> 24) & 0xff);
		b[1] = (int) ((ip >> 16) & 0xff);
		b[2] = (int) ((ip >> 8) & 0xff);
		b[3] = (int) (ip & 0xff);
		String x;
		x = Integer.toString(b[0]) + "." + Integer.toString(b[1]) + "." + Integer.toString(b[2])
				+ "." + Integer.toString(b[3]);

		return x;

	}

	public static String getIp(HttpServletRequest request) {
		String ip = request.getHeader("X-Forwarded-For");
		if(StringUtils.isEmpty(ip)  || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if(StringUtils.isEmpty(ip)  || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if(StringUtils.isEmpty(ip)  || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}

	public static String getIpByServerRequest(ServerRequest request) {
		String ip = request.headers().asHttpHeaders().getFirst("X-Forwarded-For");
		if(StringUtils.isEmpty(ip)  || "unknown".equalsIgnoreCase(ip)) {
			ip = request.headers().asHttpHeaders().getFirst("Proxy-Client-IP");
		}
		if(StringUtils.isEmpty(ip)  || "unknown".equalsIgnoreCase(ip)) {
			ip = request.headers().asHttpHeaders().getFirst("WL-Proxy-Client-IP");
		}
		if(StringUtils.isEmpty(ip)  || "unknown".equalsIgnoreCase(ip)) {
//			ip=request.headers().host().getAddress().toString();
		}
		return ip;
	}

	public static String getIpByServerRequest(ServerHttpRequest request) {
		String ip = request.getHeaders().getFirst("X-Forwarded-For");
		if(StringUtils.isEmpty(ip)  || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeaders().getFirst("Proxy-Client-IP");
		}
		if(StringUtils.isEmpty(ip)  || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeaders().getFirst("WL-Proxy-Client-IP");
		}
		if(StringUtils.isEmpty(ip)  || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddress().getHostName();
		}
		return ip;
	}
	// 测试函数
	public static void main(String[] args) throws Exception {
		long ip = ip2long("1.0.0.0");
		System.out.println(ip);
		System.out.println(long2ip(ip));

	}

}
