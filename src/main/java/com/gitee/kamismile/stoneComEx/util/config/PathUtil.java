package com.gitee.kamismile.stoneComEx.util.config;

/**
 * 得到文件存放的路径（工具类）
 *
 *
 */
public class PathUtil {

	private PathUtil() {
	}

	public static String getPath(long infoId, String root, SystemConfigVO config) {
		String rootStr = "";
		rootStr += root;
		int step = config.getStep();
		long gene = 1;
		int dimension = config.getLayer();
		for (int i = 1; i < dimension; i++) {
			gene *= step;
		}
		long tempUserID = infoId;
		for (int i = 0; i < dimension; i++) {
			if (dimension != i + 1) {
				int temp = (int) Math.ceil(tempUserID / gene);
				tempUserID = tempUserID % gene;
				rootStr += "/" + temp;
				gene /= step;
			} else {
				rootStr += "/" + infoId;
			}
		}
		return rootStr;
	}

//	public static void main(String[] rr){
//		long id = 5658;
//		SystemConfigVO vo = new SystemConfigVO();
//		vo.setLayer(8);
//		vo.setStep(300);
//		System.out.println(getPath(id, "/opt/app/media/material/img", vo));
//	}
}
