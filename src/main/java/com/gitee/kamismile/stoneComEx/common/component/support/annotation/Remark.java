package com.gitee.kamismile.stoneComEx.common.component.support.annotation;

import java.lang.annotation.*;

/**
 * Created by lidong on 15-11-10.
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Remark {
    String value() default  "";
}
