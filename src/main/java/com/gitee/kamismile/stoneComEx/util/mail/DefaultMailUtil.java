package com.gitee.kamismile.stoneComEx.util.mail;

import com.gitee.kamismile.stone.commmon.util.ValueUtils;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import jakarta.mail.internet.MimeUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;


import java.io.UnsupportedEncodingException;

public class DefaultMailUtil {
	public static final String SMTP_SNAILGAME_NET = "smtp.snailgame.net";
	public static final String USER = "sjfxz@snailgame.net";
	public static final String PASSWORD = "Snail321";
	final Logger logger = LoggerFactory.getLogger(getClass());

	private JavaMailSenderImpl mailSender;

	public DefaultMailUtil(String smtpHost,int port,String user,String password) {
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		mailSender.setHost(smtpHost);
		mailSender.setPort(port);
		mailSender.setUsername(user);
		mailSender.setPassword(password);
		this.mailSender=mailSender;
	}

	public  void sendMail(String subject, String text,boolean isHtml, String fileName, String filePath, String... addresses)
			throws MessagingException, UnsupportedEncodingException {
		MimeMessage msg = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(msg, true,"utf-8");
		helper.setFrom(mailSender.getUsername());
		helper.setTo(addresses);
		helper.setSubject(subject);
		helper.setText(text,isHtml);
		if(ValueUtils.isNotNull(fileName)&&ValueUtils.isNotNull(filePath)){
			FileSystemResource file = new FileSystemResource(filePath);
			helper.addAttachment(MimeUtility.encodeWord(fileName), file);
		}
		mailSender.send(msg);
	}


	public  void sendMailHtml(String subject, String text, String... addresses)
			throws MessagingException, UnsupportedEncodingException {
		sendMail(subject,text,true,"","",addresses);
	}

}
