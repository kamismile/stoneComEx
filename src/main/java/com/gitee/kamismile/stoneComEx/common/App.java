package com.gitee.kamismile.stoneComEx.common;

import com.gitee.kamismile.stoneComEx.common.component.support.servlet.SimpleWebConfig;
import com.gitee.kamismile.stoneComEx.common.filter.servlet.AppRequestListener;
import com.gitee.kamismile.stoneComEx.common.filter.servlet.ThreadLocalRequestAndResponseFilter;
import com.gitee.kamismile.stoneComEx.common.filter.servlet.XssFilter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.web.servlet.ConditionalOnMissingFilterBean;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.filter.OrderedCharacterEncodingFilter;
import org.springframework.boot.web.servlet.filter.OrderedRequestContextFilter;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.*;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.filter.RequestContextFilter;

//import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * Created by lidong on 15-8-26.
 */

//@EnableWebMvc
@Import(SimpleWebConfig.class)
@ComponentScan(basePackages = "com.gitee.kamismile.stoneComEx.common.*.servlet")
public class App extends SpringBootServletInitializer {

//	@Bean
//	public FilterRegistrationBean characterEncodingFilter() {
//		FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
//		CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
//		characterEncodingFilter.setEncoding("UTF-8");
//		filterRegistrationBean.setFilter(characterEncodingFilter);
//		filterRegistrationBean.setEnabled(true);
//		filterRegistrationBean.addUrlPatterns("/*");
//		return filterRegistrationBean;
//	}


    @Bean
    public CharacterEncodingFilter characterEncodingFilter() {
        CharacterEncodingFilter filter = new OrderedCharacterEncodingFilter();
        filter.setEncoding("UTF-8");
        filter.setForceRequestEncoding(true);
        filter.setForceResponseEncoding(true);
        return filter;
    }


    @Bean
    public FilterRegistrationBean requestAndResponseFilter() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        ThreadLocalRequestAndResponseFilter threadLocalRequestAndResponseFilter = new ThreadLocalRequestAndResponseFilter();
        filterRegistrationBean.setFilter(threadLocalRequestAndResponseFilter);
        filterRegistrationBean.setEnabled(true);
        filterRegistrationBean.addUrlPatterns("/*");
        return filterRegistrationBean;
    }

    @Bean
    public FilterRegistrationBean xssRequestAndResponseFilter() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        XssFilter xssFilter = new XssFilter();
        filterRegistrationBean.setFilter(xssFilter);
        filterRegistrationBean.setEnabled(true);
        filterRegistrationBean.addUrlPatterns("/*");
        return filterRegistrationBean;
    }

    @Bean
    @ConditionalOnMissingBean({RequestContextListener.class, RequestContextFilter.class})
    @ConditionalOnMissingFilterBean(RequestContextFilter.class)
    public RequestContextFilter requestContextFilter() {
        return new OrderedRequestContextFilter();
    }

//    @Bean
//    public AppRequestListener appRequestListener() {
//        return new AppRequestListener();
//    }

    @Bean
    @Primary
    @ConditionalOnClass(ReloadableResourceBundleMessageSource.class)
    @ConditionalOnMissingBean(ReloadableResourceBundleMessageSource.class)
    public ReloadableResourceBundleMessageSource messageSource() {
        ReloadableResourceBundleMessageSource rbms = new ReloadableResourceBundleMessageSource();
        rbms.setDefaultEncoding("UTF-8");
        rbms.setBasename("classpath:messages/messages");
        rbms.setUseCodeAsDefaultMessage(true);
        return rbms;
    }

//	@Bean
//	 public RequestContextListener requestContextListener(){
//		return new RequestContextListener();
//	}


//
//	@Bean
//	public WebConfig webConfig(){
//		return new WebConfig();
//	}
}
