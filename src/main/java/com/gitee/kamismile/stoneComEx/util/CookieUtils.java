package com.gitee.kamismile.stoneComEx.util;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;



/**
 * cookie操作工具类
 * @author penghy
 */
public abstract class CookieUtils {


	public static void addCookie(HttpServletResponse response, String name, String value){
	    Cookie cookie = new Cookie(name,value);
	    cookie.setPath("/");
	    response.addCookie(cookie);
	}


	public static void addCookie(HttpServletResponse response,String name,String value,String domain){
	    Cookie cookie = new Cookie(name,value);
	    cookie.setPath("/");
	    cookie.setDomain(domain);
	    response.addCookie(cookie);
	}


	public static void addCookie(HttpServletResponse response,String name,String value,String domain,String path){
	    Cookie cookie = new Cookie(name,value);
	    cookie.setPath(path);
	    if (domain != null && domain.length() != 0 && !domain.equals("*"))
	        cookie.setDomain(domain);
	    response.addCookie(cookie);
	}


	public static void addCookie(HttpServletResponse response,String name,String value,int maxAge,String domain){
	    Cookie cookie = new Cookie(name,value);
	    cookie.setPath("/");
	    if (domain != null && domain.length() != 0 && !domain.equals("*"))
	        cookie.setDomain(domain);
	    if(maxAge>0)  cookie.setMaxAge(maxAge);
	    response.addCookie(cookie);
	}


	public static Cookie getCookie(HttpServletRequest request, String name){
		Cookie[] cookies = request.getCookies();
	    if(null != cookies && cookies.length > 0){
	        for(Cookie cookie : cookies){
	            if(cookie.getName().equals(name)){
	            	return cookie;
	            }
	        }
	    }
	    return null;
	}


	public static String getCookieValue(HttpServletRequest request, String name){
		Cookie cookie = getCookie(request,name);
		if(cookie != null){
			return cookie.getValue();
		}
		return null;
	}


	public static void removeCookie(HttpServletRequest request,HttpServletResponse response,String name){
		Cookie cookie = getCookie(request,name);
		if(cookie != null){
			cookie.setMaxAge(0);
			cookie.setValue(null);
			cookie.setPath("/");
			response.addCookie(cookie);
		}
	}


	public static void removeCookie(HttpServletRequest request,HttpServletResponse response,String name,String domain){
		Cookie cookie = getCookie(request,name);
		if(cookie != null){
			cookie.setMaxAge(0);
			cookie.setValue(null);
			cookie.setPath("/");
			cookie.setDomain(domain);
			response.addCookie(cookie);
		}
	}


	public static void removeCookie(HttpServletRequest request,HttpServletResponse response,String name,String domain, String path){
		Cookie cookie = getCookie(request,name);
		if(cookie != null){
			cookie.setMaxAge(0);
			cookie.setValue(null);
			cookie.setPath(path);
			if (domain != null && domain.length() > 0 && !domain.equals("*"))
				cookie.setDomain(domain);
			response.addCookie(cookie);
		}
	}


	public static void removeAllCookie(HttpServletRequest request,HttpServletResponse response){
		Cookie[] cookies = request.getCookies();
	    if(null != cookies){
	        for(Cookie cookie : cookies){
	        	cookie.setMaxAge(0);
				cookie.setValue(null);
				response.addCookie(cookie);
	        }
	    }
	}
//#############################Cookie中文编码############################

	public static void addCookieUTF8(HttpServletResponse response, String name, String value, String domain, String path) {
		try {
			if (value != null && value.length() > 0)
				value = URLEncoder.encode(value, "UTF-8");
		} catch (UnsupportedEncodingException e) {}
	    Cookie cookie = new Cookie(name,value);
	    cookie.setPath(path);
	    if (domain != null && domain.length() != 0 && !domain.equals("*"))
	        cookie.setDomain(domain);
	    response.addCookie(cookie);
	}


	public static String getCookieValueUTF8(HttpServletRequest request, String name) {
		Cookie cookie = getCookie(request,name);
		if (cookie == null)
			return null;
		String value = cookie.getValue();
		try {
			if (value != null && value.length() > 0)
			value = URLDecoder.decode(value, "UTF-8");
		} catch (UnsupportedEncodingException e) {}
		return value;
	}
}

