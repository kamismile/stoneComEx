/**
 * LY.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package com.gitee.kamismile.stoneComEx.common.component.server;

import com.gitee.kamismile.stoneComEx.common.exception.server.OtherFluxExceptionHandler;
import com.gitee.kamismile.stoneComEx.common.exception.server.SimpleFluxException;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContext;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.i18n.SimpleLocaleContext;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.i18n.LocaleContextResolver;

import java.util.Locale;

/**
 * @author dong.li
 * @version $Id: FluxLocaleResolver, v 0.1 2019/11/7 17:56 dong.li Exp $
 */
public class FluxLocaleResolver implements LocaleContextResolver {
    private static final Logger logger =  LoggerFactory.getLogger(FluxLocaleResolver.class);

    @Value("${spring.language:zh-cn}")
    private String lang;

    @Override
    public LocaleContext resolveLocaleContext(ServerWebExchange exchange) {
        String language = exchange.getRequest().getHeaders().getFirst("custom-Language");

        Locale targetLocale =  Locale.forLanguageTag(lang);
        if (language != null && !language.isEmpty()) {
            targetLocale = Locale.forLanguageTag(language);
        }

        return new SimpleLocaleContext(targetLocale);
    }


    @Override
    public void setLocaleContext(ServerWebExchange exchange, LocaleContext localeContext) {
        LocaleContextHolder.setLocaleContext(localeContext);
    }

}
