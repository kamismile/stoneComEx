package com.gitee.kamismile.stoneComEx.common;


import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class Constant {
	protected final Logger logger =LoggerFactory.getLogger(getClass());



	public static final Integer DEFAULT_PAGE_NUM = 1;
	public static final Integer DEFAULT_PAGE_SIZE = 10;
	public static final Integer DEFAULT_TIME_LOG = 1000;

	public static final String PAGE_NUM = "pageNum";
	public static final String PAGE_SIZE = "pageSize";


	public static final String SECODE ="use des";
	public static final String GOOGLE = "Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3423.2 Mobile Safari/537.36";
}
