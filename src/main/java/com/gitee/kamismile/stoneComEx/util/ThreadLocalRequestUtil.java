package com.gitee.kamismile.stoneComEx.util;


import jakarta.servlet.http.HttpServletRequest;

public class ThreadLocalRequestUtil {
	private static ThreadLocal<HttpServletRequest> requests = new ThreadLocal<HttpServletRequest>();

	public static void setRequest(HttpServletRequest request){
		if(request!=null){
			requests.set(request);
		}
	}

	public static HttpServletRequest getRequest(){
		HttpServletRequest request = requests.get();
		return request;
	}

	public static void removeRequest(){
		requests.remove();
	}
}
